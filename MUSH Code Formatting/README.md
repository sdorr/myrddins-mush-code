## mcode.pl
A perl script that can take nicely formatted code (line breaks, indents, comments, etc) and turn it into the single block of text MUSH needs.  I find it invaluable for coding.

## mushformat.pl
A perl script that can take an arbitrary text file and 'MUSH-ify' it.
Basically, escape out what needs to be escaped, convert carriage returns to %r, convert blocks of white space, etc, so that it can be used in a MUSH and displayed properly.
