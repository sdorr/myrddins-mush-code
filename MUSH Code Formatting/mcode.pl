#!/usr/bin/env perl
#############################################################################
#
#  Title:  mcode.pl
#  Author: Myrddin (J. Scott Dorr)
#  Date:   unknown (early 00's?)
#
#  This script can take a file of formatted MUSH code and smoosh it down to
#  single blocks of text per attribute so that it can be safely quoted/pasted
#  to a MUSH.  This allows coders to write MUSH code in a more natural style.
#
#  Besides appropriate and useful line breaks, indents, blank lines, etc, you
#  can also include inline MUSH-style comments (lines beginning with '@@ ').
#  You can put line breaks whereever you'd like, as long as you keep in mind the
#  following:
#
#
#       When mcode.pl joins lines together, it does it 'tightly' ... it will not
#       try to place a space between those joined lines.
#
#  
#  Usage:       mcode.pl <filename>
#  Inside tf:   /quote -S !cat /path/to/my_formatted_code_file | ~/bin/mcode.pl
#
#  tf tip: 
#       Create a macro in tf to allow you to /quote code from a commonly used
#       location easily:
#
#               /def qc=/quote -S !cat ~/mush/code/%* | ~/bin/mcode.pl
#
#       Then, I just need to:  /qc mycodefile
#       and the file with all my easy to read formatted MUSH code is piped
#       through mcode.pl, on the fly, before being /quote'd to the MUSH.
#
#
# EXAMPLE
#
# A file can have text like the following:
#
# @@ trigger exit messages: 
# @@    set sane default succ/osucc/odrop on exit
# @@    based on its cardinal direction
# &tr_exitmsgs #134=@switch u(fn_is_cardinal_dir,%0)=
#                   0,{@pemit %1=%t:: [name(%0)] (%0) is not an exit to check},
#                   @@ default case
#                     {
#                        @pemit %1=%t:: Checking out exit [name(%0)] (%0);
#                        th setq(+,lcstr(v(d_dirname_[u(fn_dirname_from_displayname,name(%0))])),dirname);
#                        @succ %0=%%rYou head %q<dirname>.;
#                        @osucc %0=heads %q<dirname>.;
#                        @odrop %0=arrives from the [lcstr(v(d_dirname_[u(fn_rdir,left(%q<dirname>,1))]))].
#                     }
#
# 
# mcode.pl will convert it to:
#
# &tr_exitmsgs #134=@switch u(fn_is_cardinal_dir,%0)=0,{@pemit %1=%t:: [name(%0)] (%0) is not an exit to check},{@pemit %1=%t:: Checking out exit [name(%0)] (%0);th setq(+,lcstr(v(d_dirname_[u(fn_dirname_from_displayname,name(%0))])),dirname);@succ %0=%%rYou head %q<dirname>.;@osucc %0=heads %q<dirname>.;@odrop %0=arrives from the [lcstr(v(d_dirname_[u(fn_rdir,left(%q<dirname>,1))]))].}
#
#############################################################################
$debug = 0;
$blankline = 0;

while($l = <>)
{
   chomp $l;
   if ($l =~ /^$/) {
      $blankline++;
      next;
   }

print ":: not a blank line\n" if $debug;

   # if ($l =~ /^\Q@@ \E/) {
   # if ($l =~ /^@@ /) {
   #    &print_blanklines();
   #    if ($codeline) {
   #       print $codeline, "\n";
   #       $codeline = '';
   #    }
   #    print "$l\n";
   #    next;
   # }

print ":: not a comment\n" if $debug;

   # something other than whitespace.  this is the start of a new line of code
   if ($l =~ /^\S/) {
      if ($codeline) {
         print $codeline, "\n";
      }
      &print_blanklines();
      $codeline = $l;
      next;
   }

print ":: not an new line of code\n" if $debug;

   # must be an extension of the current code line
   if ($codeline) {
      my $tmp = $l;
      $tmp =~ s/^\s+//;
      $tmp =~ s/\s+$//;

      if ($tmp eq '@@' or $tmp =~ /^@@ /) {
         # inline comment.  ignore entirely
      }
      else {
         $codeline .= $tmp;
      }
   }
   else {
      # huh?
      print "@@ ERROR: poorly formatted mush test file at line: ($l)\n";
   }

   $blankline = 0;
}

&print_blanklines();
if ($codeline) {
   print $codeline, "\n";
}

sub print_blanklines()
{
   $bls = "\n" x $blankline;
   $blankline = 0;

   print $bls;
}
