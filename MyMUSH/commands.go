// vim:tw=9999
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"runtime"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

var con_cmds map[string]func(net.Conn, con_cmd_options) *Player
var player_cmds map[string]func(*Player, player_cmd_options) *Player

func init_cmds() {
	log.Printf("initializing connection command table")
	con_cmds = map[string]func(net.Conn, con_cmd_options) *Player{
		"connect": cmd_connect,
		"create":  cmd_create,
		"who":     cmd_who_connect,
	}

	player_cmds = map[string]func(*Player, player_cmd_options) *Player{
		"look":         cmd_look,
		"l":            cmd_look,
		"pose":         cmd_pose,
		"say":          cmd_say,
		"th":           cmd_think,
		"@newpassword": cmd_newpassword,
		"who":          cmd_who,
	}
}

type player_cmd_options struct {
	player   Player
	cmd      string
	cmd_args string
}

type con_cmd_options struct {
	name string
	pw   string
}

func cmd_look(player *Player, opts player_cmd_options) *Player {
	p := *player
	room, _ := db_getroom(p.Location)

	netWrite(player.Conn, "\n%v (#%v)\n%v\n\n", room.Name, room.DBref, room.Desc)

	ctx := context.TODO()
	cursor, err := mongoPlayers.Find(ctx, bson.D{{"location", p.Location}})
	if err != nil {
		// TODO: emote to the player at least?
		log.Fatal(err)
	}

	do_contents := true
	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		if tmpPlayer.DBref != p.DBref {
			if _, ok := players[tmpPlayer.DBref]; ok {
				tp := players[tmpPlayer.DBref]
				if tp.State == ps_LoggedIn {
					if do_contents == true {
						netWrite(p.Conn, "Contents:\n")
						do_contents = false
					}
					netWrite(p.Conn, "%v\n", tmpPlayer.Name)
				}
			}
		} // if not looker
	}

	return player
}

func cmd_pose(player *Player, opts player_cmd_options) *Player {
	p := *player

	space := " "
	if opts.cmd == "semipose" {
		space = ""
	}
	msg := fmt.Sprintf("%v%v%v", p.Name, space, opts.cmd_args)
	emote_room(p.Location, msg, -1)

	return player
}

func cmd_say(player *Player, opts player_cmd_options) *Player {
	p := *player

	netWrite(p.Conn, "You say, \"%v\"\n", opts.cmd_args)

	msg := fmt.Sprintf("%v says, \"%v\"", p.Name, opts.cmd_args)
	emote_room(p.Location, msg, p.DBref)

	return player
}

func cmd_think(player *Player, opts player_cmd_options) *Player {
	p := *player
	netWrite(p.Conn, "%v\n", opts.cmd_args)
	return player
}

func cmd_newpassword(player *Player, opts player_cmd_options) *Player {
	p := *player
	err := db_SetPW(player, opts.cmd_args)
	if err != nil {
		netWrite(p.Conn, "ERROR: Unable to change password\n")
		player.Password = opts.cmd_args
	} else {
		netWrite(p.Conn, "Password changed to '%v'\n", opts.cmd_args)
	}
	return player
}

func cmd_connect(c net.Conn, opts con_cmd_options) *Player {
	var player Player

	netWrite(c, ":: attempting connection to %v\n", opts.name, opts.pw)

	// collection := mongoclient.Database(config.MongoDB).Collection("players")
	// filter := bson.D{{"name",opts.name}, {"password",opts.pw}}
	// filter := bson.M{"name": opts.name, "password": opts.pw}
	// rpat := fmt.Sprintf("^%v$",opts.name)
	// filter := bson.D{{Key:"name", Value:bson.D{{"$regex", primitive.Regex{Pattern:rpat, Options:"i"}}}}}
	// filter = append(filter, bson.E{"password", opts.pw})

	filter := bson.M{"lcname": strings.ToLower(opts.name)}

	netWrite(c, "filter: :%v:\n", filter)

	err := mongoPlayers.FindOne(context.TODO(), filter).Decode(&player)

	if err != nil {
		netWrite(c, ":: That player/password combination is invalid\n")
		player.State = ps_ConnScreen
		return &player
	}

	if player.Password != opts.pw {
		netWrite(c, ":: bad pw\n")
		player.State = ps_ConnScreen
		return &player
	}

	netWrite(c, ":: Success! (loc: %v, dbref: %v)\n", player.Location, player.DBref)
	player.Conn = c
	player.State = ps_LoggedIn
	players[player.DBref] = &player
	return &player
}

func cmd_create(c net.Conn, opts con_cmd_options) *Player {

	dbref := get_next_dbref()
	player := Player{
		Name:     opts.name,
		Password: opts.pw,
		DBref:    dbref,
		Desc:     "",
		Location: 0}

	// TODO: make sure player name doesn't already exist
	// TODO: dbref generation

	log.Printf("cmd_create: creating '%v' with dbref '#%v'\n", opts.name, dbref)

	db_AddPlayer(&player)
	return cmd_connect(c, opts)
}

// Player Name        On For Idle   Room    Cmds   Host
// Myrddin          1d 03:03   0s$  #0         1   178.128.180.193
// Adrick          58d 17:40   1m   #0     16921   35.238.239.137
// Oleo            58d 19:13   5d$  #4        17   jabberwock.mushpark.com
// 3 Players logged in, 4 record, no maximum.
// func cmd_who(c net.Conn, opts player_cmd_options) *Player {
func cmd_who(player *Player, opts player_cmd_options) *Player {
	// func Caller(skip int) (pc uintptr, file string, line int, ok bool)

	if player == nil {
		funcname, filename, line, _ := runtime.Caller(0)
		log.Printf("%v: %v: %v: NULL player", filename, funcname, line)
		return nil
	}
	plural := "s"
	c := (*player).Conn
	i := 0

	netWrite(c, "%-20sdbref\n", "Player Name")
	for dbref, player := range players {
		// fmt.Printf("key[%s] value[%s]\n", k, v)
		netWrite(c, "%-20s#%d\n", player.Name, dbref)
		i++
	}

	if i == 1 {
		plural = ""
	}

	netWrite(c, "%d player%s logged in.\n", i, plural)

	return player
}

func cmd_who_connect(c net.Conn, opts con_cmd_options) *Player {
	var p Player
	p.Conn = c
	return cmd_who(&p, player_cmd_options{})
}
