// vim:tw=99999
package main

import (
	"context"
	// "fmt"
	"log"
	"net"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/primitive"
)

type GameMetaData struct {
	next_dbref int
}

type COMMAND_GOCOMM int

const (
	cmd_gc_shutdown COMMAND_GOCOMM = iota
)

type goComm struct {
	cmd COMMAND_GOCOMM
}

type Player struct {
	_id      primitive.ObjectID
	Name     string
	Password string
	DBref    int
	Location int
	Desc     string
	Conn     net.Conn
	State    PLAYER_STATE
	Cmds     int
}

type Room struct {
	_id   primitive.ObjectID
	Name  string
	DBref int
	Owner int
	Desc  string
}

// global maps
var players map[int]*Player
var gameData GameMetaData

func init_players_map() {
	log.Printf("iniitializing player map")
	players = make(map[int]*Player)
}

func get_next_dbref() int {
	dbref := gameData.next_dbref
	gameData.next_dbref++
	return dbref
}

func emote_room(loc int, msg string, skip int) {
	ctx := context.TODO()
	cursor, err := mongoPlayers.Find(ctx, bson.D{{"location", loc}})
	if err != nil {
		// TODO: emote to the player at least?
		log.Fatal(err)
	}

	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		if tmpPlayer.DBref != skip {
			if _, ok := players[tmpPlayer.DBref]; ok {
				tp := players[tmpPlayer.DBref]
				if tp.State == ps_LoggedIn && tp.Conn != nil {
					netWrite(players[tmpPlayer.DBref].Conn, "%v\n", msg)
				}
			}
			// log.Printf("cmd_say: #%v:%v\n", tmpPlayer.DBref, tmpPlayer.Name)
		}
	}
}
