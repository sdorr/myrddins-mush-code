package main

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// type YamlConfig struct {
//     Port int    `yaml:"port"`
//     Name string `yaml:"name"`
//     Mongo struct {
//         Host    string  `yaml:"host"`
//         DB      string  `yaml:"db"`
//     }
// }

// type YamlConfig struct {
//     Port    int             `yaml:"port"`
//     Name    string          `yaml:"name"`
//     Mongo   MongoConfig     `yaml:"mongo"`
// }
//
// type MongoConfig struct {
//     host    string          `yaml:"host"`
//     db      string          `yaml:"db"`
// }

type YamlConfig struct {
	Port      int    `yaml:"port"`
	Name      string `yaml:"name"`
	MongoHost string `yaml:"MongoHost"`
	MongoDB   string `yaml:"MongoDB"`
}

var config YamlConfig

func load_config(cfile string) YamlConfig {

	//
	// load config file
	//
	yamlFile, err := ioutil.ReadFile(cfile)
	if err != nil {
		log.Printf("Error reading config file: %s\n", err)
		os.Exit(1)
	}

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Printf("Error parsing config file: %s\n", err)
	}

	// log.Printf("Config: %+v\n", config)
	log.Printf("Config:\n")
	log.Printf("\tName: %v\n", config.Name)

	if config.Port == 0 {
		config.Port = 4444
		log.Printf("\tPort: %v (default)\n", config.Port)
	} else {
		log.Printf("\tPort: %v\n", config.Port)
	}

	log.Printf("\tMongoHost: %v\n", config.MongoHost)
	log.Printf("\tMongoDB:   %v\n", config.MongoDB)

	return config
}
