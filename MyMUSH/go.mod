module bitbucket.org/myrddin0/MyMUSH

go 1.13

require (
	go.mongodb.org/mongo-driver v1.3.0
	gopkg.in/yaml.v2 v2.2.8
)
