// vim:tw=9999
package main

import (
	"context"
	"errors"
	"log"
	"os"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongoclient *mongo.Client
var mongoDB *mongo.Database
var mongoPlayers *mongo.Collection
var mongoThings *mongo.Collection
var mongoRooms *mongo.Collection
var mongoExits *mongo.Collection
var mongoMeta *mongo.Collection

func connect_mongo(host string) *mongo.Client {
	URI := "mongodb://" + host + ":27017"
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(URI))
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	mongoclient = client

	log.Printf("successful mongodb connection: %v", URI)

	return mongoclient
}

func db_set_globals(db *mongo.Database) {
	mongoDB = db
	mongoPlayers = db.Collection("players")
	mongoThings = db.Collection("things")
	mongoRooms = db.Collection("rooms")
	mongoExits = db.Collection("exits")
	mongoMeta = db.Collection("meta")
}

func init_db(mc *mongo.Client, dbname string) {
	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", dbname}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		// log.Printf("\t%v\n", db)
		if db == dbname {
			log.Printf("%v database exists", dbname)
			db_set_globals(mc.Database(dbname))
			return
		}
	}

	log.Printf("%v database not found.  Generating fresh database.\n", dbname)
	db_set_globals(mc.Database(dbname))

	// create room #0
	res, err := mongoRooms.InsertOne(context.Background(), bson.M{"dbref": 0,
		"name":  "Limbo",
		"owner": 1,
		"desc":  "A misty realm that seems to stretch to infinity."})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	limbo_id := res.InsertedID

	gameData.next_dbref = 1

	// create Wizard
	wiz := Player{
		Name:     "Wizard",
		Password: "potrzebie",
		DBref:    get_next_dbref(),
		Desc:     "",
		Location: 0}
	db_AddPlayer(&wiz)

	db_UpdateDBrefs()

	log.Printf("new %v database initialized with Limbo (%v) and Wizard (%v)\n", dbname, limbo_id, wiz._id)
}

func db_AddPlayer(player *Player) (*Player, error) {
	p := *player
	var err error

	res, err := mongoPlayers.InsertOne(context.Background(), bson.M{
		"dbref":    p.DBref,
		"name":     p.Name,
		"lcname":   strings.ToLower(p.Name),
		"password": p.Password,
		"desc":     "",
		"location": 0})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	player._id = res.InsertedID.(primitive.ObjectID)
	db_UpdateDBrefs()
	return player, nil
}

func db_SetPW(player *Player, newpw string) error {
	p := *player
	opts := options.Update().SetUpsert(false)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"dbref", p.DBref}}
	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

	log.Printf("attempting to change password for '%v (#%v)' (%v)\n", p.Name, p.DBref, p._id)
	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Printf("player '%v (#%v)' just changed password", p.Name, p.DBref)
	} else {
		log.Printf("WARNING: Unable to change password for player '%v (#%v)'", p.Name, p.DBref)
		return errors.New("can't update document")
	}

	return nil
}

func list_dbs(mc *mongo.Client) {
	log.Printf("listing mongo databases:\n")
	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{})
	// result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", "eternal_mists"}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		log.Printf("\t%v\n", db)
	}
}

func db_LoadMetaData() error {
	filter := bson.M{"type": "database"}
	err := mongoMeta.FindOne(context.TODO(), filter).Decode(&gameData)
	return err
}

//	filter := bson.D{{"dbref", p.DBref}}
//	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

//	log.Printf("attempting to change password for '%v (#%v)' (%v)\n", p.Name, p.DBref, p._id)
//	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)

func db_UpdateDBrefs() error {
	opts := options.Update().SetUpsert(true)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"type", "database"}}
	update := bson.D{{"$set", bson.D{{"next_dbref", gameData.next_dbref}}}}

	result, err := mongoMeta.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount != 1 {
		log.Printf("ERROR: Unable to update next_dbref in mongo")
		return errors.New("can't update document")
	}

	return nil
}

func db_getroom(dbref int) (Room, error) {
	var room Room

	// collection := mongoclient.Database(config.MongoDB).Collection("rooms")

	filter := bson.M{"dbref": dbref}
	err := mongoRooms.FindOne(context.TODO(), filter).Decode(&room)
	if err != nil {
		log.Printf("invalid room specifier: %v\n", dbref)
	}

	return room, err
}
