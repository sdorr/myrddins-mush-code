// vim:tw=99999
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	// "os"
	// "strconv"
	"strings"
	"time"
	// "gopkg.in/yaml.v2"
)

type PLAYER_STATE int

const (
	ps_ConnScreen PLAYER_STATE = iota
	ps_LoggedIn
	ps_Disconnecting
	ps_Disconnected
)

func main() {

	var config_file string

	//
	// parse command line
	//
	flag.StringVar(&config_file, "f", "", "config file (yaml)")
	flag.Parse()

	if config_file == "" {
		config_file = "mymush.conf"
		log.Printf("Using default config file: %v (override with -f <config>)\n", config_file)
	} else {
		log.Printf("config file: %v\n", config_file)
	}

	//
	// load config file
	//
	load_config(config_file)

	mongoclient = connect_mongo(config.MongoHost)

	init_db(mongoclient, config.MongoDB)
	init_cmds()
	init_players_map()

	//
	// start up listening port
	//
	sock := netStartMainSock()

	rand.Seed(time.Now().Unix())

	for {
		conn, err := sock.Accept()
		if err != nil {
			log.Println(err)
			return
		}
		go handleConnection(conn)
	}

	log.Printf("Shutting down")
}

func handleConnection(c net.Conn) {
	var pplayer *Player
	state := ps_ConnScreen

	log.Printf("connection: %s -> %v\n", c.RemoteAddr().String(), c.LocalAddr())

	conntxt, _ := ioutil.ReadFile("dat/connect.txt")
	netWriteB(c, conntxt)

	reader := bufio.NewReader(c)
	for {
		// netData, err := bufio.NewReader(c).ReadString('\n')
		netData, err := reader.ReadString('\n')
		if err != nil {
			log.Printf("%v: %v\n", c.RemoteAddr().String(), err)
			state = ps_Disconnecting
			break
		}

		cmdstr := strings.TrimSpace(string(netData))
		if cmdstr == "QUIT" {
			state = ps_Disconnecting
			break
		}

		parts := strings.Split(cmdstr, " ")
		cmd := strings.ToLower(parts[0])

		if _, ok := con_cmds[cmd]; ok {
			arg_name := ""
			arg_pw := ""
			if len(parts) >= 3 {
				arg_pw = parts[2]
			}
			if len(parts) >= 2 {
				arg_name = parts[1]
			}

			pplayer = con_cmds[cmd](c, con_cmd_options{name: arg_name, pw: arg_pw})
		} else {
			netWrite(c, "\n>>> Invalid command '%v' <<<\n\n", cmd)
			netWriteB(c, conntxt)
		}

		netWrite(c, "\n")
		if (pplayer != nil) && ((*pplayer).State != ps_ConnScreen) {
			break
		}
	}

	player := *pplayer
	if pplayer != nil {
		state = player.State
	}

	if state == ps_Disconnecting {
		txt, _ := ioutil.ReadFile("dat/disconnect.txt")
		netWriteB(c, txt)

		log.Printf("disconnection: %s\n", c.RemoteAddr().String())
		c.Close()
	}

	room, _ := db_getroom(player.Location)

	log.Printf("%v (#%v) connected in room '%v (#%v)'\n",
		player.Name, player.DBref, room.Name, room.DBref)

	msg := fmt.Sprintf("%v has connected.", player.Name)
	emote_room(player.Location, msg, player.DBref)
	// log.Printf("player:\n\n%v\n\n", player)
	// netWrite(player.Conn, "\n%v (#%v)\n%v\n\n", room.Name, room.DBref, room.Desc)
	cmd_look(pplayer, player_cmd_options{cmd: "look", cmd_args: ""})

	// forever loop for player
	for {
		netData, err := reader.ReadString('\n')
		if err != nil {
			log.Printf("%v: %v\n", c.RemoteAddr().String(), err)
			break
		}

		cmdstr := strings.TrimSpace(string(netData))
		if cmdstr == "QUIT" {
			break
		}

		if cmdstr[0] == '"' {
			// netWrite(c, "%v says, \"%v\"\n", player.Name, cmdstr[1:])
			cmd_say(pplayer, player_cmd_options{cmd: "say", cmd_args: cmdstr[1:]})
		} else if cmdstr[0] == ':' {
			// netWrite(c, "%v %v\n", player.Name, cmdstr[1:])
			cmd_pose(pplayer, player_cmd_options{cmd: "pose", cmd_args: cmdstr[1:]})
		} else if cmdstr[0] == ';' {
			// netWrite(c, "%v%v\n", player.Name, cmdstr[1:])
			cmd_pose(pplayer, player_cmd_options{cmd: "semipose", cmd_args: cmdstr[1:]})
		} else {
			cmdv := strings.SplitN(cmdstr, " ", 2)
			cmd := strings.ToLower(cmdv[0])
			if _, ok := player_cmds[cmd]; ok {
				if len(cmdv) > 1 {
					pplayer = player_cmds[cmd](pplayer, player_cmd_options{cmd: cmd, cmd_args: cmdv[1]})
				} else {
					pplayer = player_cmds[cmd](pplayer, player_cmd_options{cmd: cmd})
				}
			} else {
				netWrite(c, "(Huh?)\n")
			}
		}

		if (pplayer != nil) && ((*pplayer).State == ps_Disconnecting) {
			break
		}
	}
	msg = fmt.Sprintf("%v has disconnected.", player.Name)
	player.State = ps_LoggedIn
	emote_room(player.Location, msg, player.DBref)

	log.Printf("%v (#%v) disconnected: %s\n", player.Name, player.DBref, c.RemoteAddr().String())
	c.Close()
}
