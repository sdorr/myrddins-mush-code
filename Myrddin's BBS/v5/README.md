Myrddin's Global Bulletin Board, v5
===================================

After many years, the BBS has an official update which includes threaded replies, color support, an intelligent +bbnext, and other fixes/modifications.
It's been running successfully on active Penn and MUX games, as well as multiple development Rhost games.  Spot testing has been done on Tiny as well.

## INSTALL
---
1. Click on the [myrddin_bb.v5.code](https://bitbucket.org/myrddin0/myrddins-mush-code/src/master/Myrddin's%20BBS/v5/myrddin_bb.v5.code) file above.
2. Follow the simple instructions at the top of the file depending on whether or not you have an existing BBS.
3. Copy and paste the code to your game as is.


## CHANGES
---

## 5.0.1:

### BUGFIXES
-   A nearly full (>90%) would cause some commands to error out to to excess
    spaces embedding themselves in certain temp buffers.

## 5.0.0:

### +bbreply
-   Feature: new command, '+bbreply' will allow people to reply to a specific
    board message.  This resuls in an in-line reply with a new numbering scheme
    for replies as well as a visual, threaded cue in the message list that
    makes replies obvious.
-   Replies to replies will become replies to the parent (no nested replies;
    those are too unwieldy for a text based interface with limited
    real-estate).
-   Due to the new numbering scheme for replies, pre-existing messages will not
    have their message number changed (eg. if a board has 16 posts, and someone
    does a reply to post 5, the reply will be 5.1, leaving posts 6-16
    unchanged).

### Colors
-   Feature: The bbs supports optional and customizable colors
-   Toggle colors on/off with +bbconfig
-   Three different colors used for different parts of the output, each color can be configured
-   Color coded flags (Unread, Timeout warning, etc)

### Timestamp changes
-   Feature: the year now appears in bb postings
-   Feature: HH:MM:SS will also appear in specific message readings
-   Feature: better header layout to accomodate the change in date
-	Feature: timestamp stored internally as epoch, to allow for future proofing of any changes in how we want to display the timestamp

### +bbnext
-   Feature: +bbnext will show 'next' unread message
-   Feature: '+bbnext <#>' will show 'next' unread message in board <#>

### Misc
-   Subject lines can now be up to 64 characters in length (previous max was 34)

### BUGFIXES
-   '+bbread <group>' on an empty group will no longer have an awkward, truncated output.
