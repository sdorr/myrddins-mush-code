#!/usr/bin/env perl
# vim:tw=99999:number

# you don't need this script
#
# stupid script to flatten my raw, formatted file into the flattened MUSH code file for quoting.

$VER = $ARGV[0] || `grep -m1 _myrbb_version *raw | cut -f2 -d'='`;
chomp $VER;

$HELPROOT="myrddin_bb.v5.help";
$TEMPLATE="${HELPROOT}.template";
$HELPFILE="../${HELPROOT}";
$MUSHFILE="${HELPFILE}.mush";

print "Creating $HELPFILE from $TEMPLATE\n";

    open($tempfh,$TEMPLATE);
    read $tempfh, my $helpdata, -s $tempfh;
    close $tempfh;

    $helpdata =~ s/BBVER/$VER/g;

    open($hfh,">$HELPFILE");
    print $hfh $helpdata;
    close $hfh;

print "Creating $MUSHFILE from $HELPFILE\n";

$flat = `mushformat $HELPFILE`;

@topics = split '& bb', $flat;

open(MF, ">$MUSHFILE");
foreach $topic (@topics) {
    next if $topic =~ m/^$/;
    $topic =~ s/^([a-z0-9]*)%r==============================================================================%r//;
    if ($1 eq 'oard') { $attr = '&HELP_BBS #15='; }
                 else { $attr = '&HELP_BB' . uc($1) . '_HIDDEN #15='}
    $topic = $attr . $topic;
    $topic =~ s/%r==============================================================================%r%r$//;
    $topic =~ s/%r----------/%r------------/g;
    print MF "\n$topic\n";
}



# oard%r==============================================================================%r[space(16)]Commands for Myrddin's Bulletin Board 5.0.0%r------------------------------------------------------------------------------%r[space(5)]Requiem's BBS is a global, multi-threaded board with a rich set of%r[space(5)]features. To see help on a particular topic, type '+help <topic>'%r[space(5)](Example: +help bbread).%r%b%b%r[space(5)]TOPIC[space(17)]DESCRIPTION%r[space(5)]~~~~~[space(17)]~~~~~~~~~~~%r[space(5)]bbread[space(16)]Reading bulletin board messages.%r[space(5)]bbpost[space(16)]Posting bulletin board messages.%r[space(5)]bbcolors[space(14)]Colors!%r[space(5)]bbmisc[space(16)]Other commands (removing messages, unsubscribing%r[space(29)]groups, resubscribing to groups, etc)%r%b%b%r[space(5)]bbtimeout[space(13)]Expanded help on the topic of message timeouts.%r==============================================================================%r%r
