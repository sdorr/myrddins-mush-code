# Myrddin's mushcron, v1.0.0

## What is it?

mushcron is a soft coded mush utility used to trigger events regularly at pre-specified times. It's based on the unix cron utility. You can be as specific or generic as you need. If you want something triggered every 10 minutes, if you want something triggered every night at 3:05am, or if you want something triggered every odd hour on every Thursday in the month of June, it's all possible and quite easy to set up.

## What can I use it for?

There's no way I could come up with a comprehensive list, the uses are vast, and are specific to the needs of your game. Some things I've used mushcron for:

*   Automated nightly shutdown of my mush. (perl scripts backup and restart the game from there)
*   Bulletin Board message timeouts.
*   Experience point accounting on a daily and weekly basis.
*   Keeping track of total time accumulated for players and staff on a weekly and 'since inception' basis.
*   Healing/Damage accounting.

Some other uses I can envision off the top of my head:

*   Weather emits
*   Virtual time emits
*   OOC Room cleanup
*   Nuking of old, unused, incomplete characters (idle-desting)

Copyright ©1996-2020 J. Scott Dorr
